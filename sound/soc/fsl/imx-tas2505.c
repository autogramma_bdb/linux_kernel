/*
 * 2020 Autogramma LLC. All Rights Reserved.
 *
 * Based on imx-tlv320aic3x.c
 * Copyright (C) 2014 Variscite, Ltd.
 * Copyright (C) 2014 Freescale Semiconductor, Inc.
 * Copyright (C) 2012 Linaro Ltd.
 *
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <linux/module.h>
#include <linux/of_platform.h>
#include <linux/i2c.h>
#include <sound/soc.h>

#include "imx-audmux.h"

#define DAI_NAME_SIZE	32

struct imx_tas2505_data {
	struct snd_soc_dai_link dai;
	struct snd_soc_card card;
	char codec_dai_name[DAI_NAME_SIZE];
	char platform_name[DAI_NAME_SIZE];
};

static int imx_audmux_config(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	int int_port, ext_port;
	int ret;

	ret = of_property_read_u32(np, "mux-int-port", &int_port);
	if (ret) {
		dev_err(&pdev->dev, "mux-int-port missing or invalid\n");
		return ret;
	}
	ret = of_property_read_u32(np, "mux-ext-port", &ext_port);
	if (ret) {
		dev_err(&pdev->dev, "mux-ext-port missing or invalid\n");
		return ret;
	}

	/*
	 * The port numbering in the hardware manual starts at 1, while
	 * the audmux API expects it starts at 0.
	 */
	int_port--;
	ext_port--;

	ret = imx_audmux_v2_configure_port(ext_port,
		IMX_AUDMUX_V2_PTCR_SYN |
		IMX_AUDMUX_V2_PTCR_TFSEL(int_port) |
		IMX_AUDMUX_V2_PTCR_TCSEL(int_port) |
		IMX_AUDMUX_V2_PTCR_TFSDIR |
		IMX_AUDMUX_V2_PTCR_TCLKDIR,
		IMX_AUDMUX_V2_PDCR_RXDSEL(int_port));
	if (ret) {
		dev_err(&pdev->dev, "audmux internal port setup failed\n");
		return ret;
	}
	ret = imx_audmux_v2_configure_port(int_port,
		IMX_AUDMUX_V2_PTCR_SYN,
		IMX_AUDMUX_V2_PDCR_RXDSEL(ext_port));

	if (ret) {
		dev_err(&pdev->dev, "audmux external port setup failed\n");
		return ret;
	}
	return 0;
}

static const struct snd_soc_dapm_widget imx_tas2505_dapm_widgets[] = {
    	SND_SOC_DAPM_SPK("Ext Spk", NULL),
};

static int imx_tas2505_probe(struct platform_device *pdev)
{
	struct device_node *cpu_np, *codec_np;
	struct platform_device *cpu_pdev;
	struct i2c_client *codec_dev;
	struct imx_tas2505_data *data;
	int ret;

	data = devm_kzalloc(&pdev->dev, sizeof(*data), GFP_KERNEL);
	if (!data) {
		ret = -ENOMEM;
		goto fail;
	}

	platform_set_drvdata(pdev, data);

	cpu_np = of_parse_phandle(pdev->dev.of_node, "cpu-dai", 0);
	codec_np = of_parse_phandle(pdev->dev.of_node, "audio-codec", 0);
	if (!cpu_np || !codec_np) {
		dev_err(&pdev->dev, "phandle missing or invalid\n");
		ret = -EINVAL;
		goto fail;
	}

	if (strstr(cpu_np->name, "ssi")) {
		ret = imx_audmux_config(pdev);
		if (ret)
			goto fail;
	}

	cpu_pdev = of_find_device_by_node(cpu_np);
	if (!cpu_pdev) {
		dev_err(&pdev->dev, "failed to find SSI platform device\n");
		ret = -EINVAL;
		goto fail;
	}
	codec_dev = of_find_i2c_device_by_node(codec_np);
	if (!codec_dev) {
		dev_err(&pdev->dev, "failed to find codec platform device\n");
		return -EINVAL;
	}

	data->dai.name = "HiFi";
	data->dai.stream_name = "HiFi";
	data->dai.codec_dai_name = "tas2505-hifi";
	data->dai.cpu_dai_name = dev_name ( &cpu_pdev->dev );
	data->dai.codec_of_node = codec_np;
	data->dai.cpu_of_node = cpu_np;
	data->dai.platform_of_node = cpu_np;
	data->dai.dai_fmt = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_NB_NF |
			    SND_SOC_DAIFMT_CBS_CFS;

	data->card.dev = &pdev->dev;
	ret = snd_soc_of_parse_card_name(&data->card, "model");
	if (ret)
		goto fail;

	ret = snd_soc_of_parse_audio_routing(&data->card, "audio-routing");
	if (ret)
		goto fail;
	data->card.num_links = 1;
	data->card.owner = THIS_MODULE;
	data->card.dai_link = &data->dai;
	data->card.dapm_widgets = imx_tas2505_dapm_widgets;
	data->card.num_dapm_widgets = ARRAY_SIZE(imx_tas2505_dapm_widgets);

	ret = snd_soc_register_card(&data->card);
	if (ret) {
		dev_err(&pdev->dev, "snd_soc_register_card failed (%d)\n", ret);
		goto fail;
	}

fail:
	if (cpu_np)
		of_node_put(cpu_np);
	if (codec_np)
		of_node_put(codec_np);

	return ret;

}

static int imx_tas2505_remove(struct platform_device *pdev)
{
	struct imx_tas2505_data *data = platform_get_drvdata(pdev);

	snd_soc_unregister_card(&data->card);

	return 0;
}

static const struct of_device_id imx_tas2505_dt_ids[] = {
	{ .compatible = "fsl,imx-audio-tas2505", },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, imx_tas2505_dt_ids);

static struct platform_driver imx_tas2505_driver = {
	.driver = {
		.name = "imx-amp-tas2505",
		.owner = THIS_MODULE,
		.of_match_table = imx_tas2505_dt_ids,
	},
	.probe = imx_tas2505_probe,
	.remove = imx_tas2505_remove,
};
module_platform_driver(imx_tas2505_driver);

MODULE_AUTHOR("Alibek Omarov");
MODULE_DESCRIPTION("ALSA SoC i.MX tas2505");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:imx-tas2505");
