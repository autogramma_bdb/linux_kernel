/*
Copyright (C) 2020 Autogramma LLC.

DTS example:
sound_max {
	compatible = "fsl,imx-audio-max98357a";
	model = "imx-amp-max98357a";
	cpu-dai = <&ssi3>;
	audio-codec = <&codec_max>;
	audio-routing =
		"Ext Spk",	"Speaker";
	mux-int-port = <7>;
	mux-ext-port = <4>;
};

codec_max: max98357a {
	compatible = "maxim,max98357a";
	gpio-sdmode = <&gpio4 9 1>;
};
*/

#include <linux/module.h>
#include <linux/of_platform.h>
#include <linux/i2c.h>
#include <sound/soc.h>
#include <linux/clk.h>

#include "imx-audmux.h"

#define DAI_NAME_SIZE	32

struct imx_max98357a_data {
	struct snd_soc_dai_link dai;
	struct snd_soc_card card;
	char codec_dai_name[DAI_NAME_SIZE];
	char platform_name[DAI_NAME_SIZE];
	struct gpio_desc *output_enable_gpio;
};

static int imx_audmux_config(struct platform_device *pdev){
	struct device_node *np = pdev->dev.of_node;
	int int_port, ext_port;
	unsigned int ptcr, pdcr;
	int ret;

	ret = of_property_read_u32(np, "mux-int-port", &int_port);
	if (ret) {
		dev_err(&pdev->dev, "mux-int-port missing or invalid\n");
		return ret;
	}
	ret = of_property_read_u32(np, "mux-ext-port", &ext_port);
	if (ret) {
		dev_err(&pdev->dev, "mux-ext-port missing or invalid\n");
		return ret;
	}

	int_port--;
	ext_port--;

	ptcr = 0;
	pdcr = 0;
	pdcr = IMX_AUDMUX_V2_PDCR_RXDSEL(ext_port);

	imx_audmux_v2_configure_port(int_port, ptcr, pdcr);
	if (ret) {
		dev_err(&pdev->dev, "audmux internal port setup failed\n");
		return ret;
	}

	ptcr = 0;
	pdcr = 0;
	ptcr = 	(IMX_AUDMUX_V2_PTCR_TFSEL(int_port) |
		IMX_AUDMUX_V2_PTCR_TCSEL(int_port) |
		IMX_AUDMUX_V2_PTCR_TFSDIR |
		IMX_AUDMUX_V2_PTCR_TCLKDIR);
	pdcr = IMX_AUDMUX_V2_PDCR_RXDSEL(int_port);

	imx_audmux_v2_configure_port(ext_port, ptcr, pdcr);
	if (ret) {
		dev_err(&pdev->dev, "audmux external port setup failed\n");
		return ret;
	}

	return 0;
}

static const struct snd_soc_dapm_widget imx_max98357a_dapm_widgets[] = {
    	SND_SOC_DAPM_SPK("Ext Spk", NULL),
};

static int imx_max98357a_probe(struct platform_device *pdev){
	
	struct device_node *cpu_np, *codec_np;
	struct platform_device *cpu_pdev;
	struct imx_max98357a_data *data;
	struct gpio_desc *gpio;
	int ret;

	cpu_np = of_parse_phandle(pdev->dev.of_node, "cpu-dai", 0);

	if (strstr(cpu_np->name, "ssi")) {
		ret = imx_audmux_config(pdev);
		if (ret)
			goto fail;
	}

	cpu_pdev = of_find_device_by_node(cpu_np);
	if (!cpu_pdev) {
		dev_err(&pdev->dev, "failed to find SSI platform device\n");
		ret = -EINVAL;
		goto fail;
	}

	codec_np = of_parse_phandle(pdev->dev.of_node, "audio-codec", 0);

	if (!cpu_np || !codec_np) {
		dev_err(&pdev->dev, "phandle missing or invalid\n");
		ret = -EINVAL;
		goto fail;
	}

	data = devm_kzalloc(&pdev->dev, sizeof(*data), GFP_KERNEL);
	if (!data) {
		ret = -ENOMEM;
		goto fail;
	}

	data->dai.name = "HiFi";
	data->dai.stream_name = "HiFi";
	data->dai.codec_dai_name = "max98357a-hifi";
	data->dai.codec_of_node = codec_np;
	data->dai.cpu_of_node = cpu_np;
	data->dai.platform_of_node = cpu_np;
	data->dai.dai_fmt = SND_SOC_DAIFMT_I2S | SND_SOC_DAIFMT_CBS_CFS;

	data->card.dev = &pdev->dev;

	ret = snd_soc_of_parse_card_name(&data->card, "model");
	if (ret)
		goto fail;

	ret = snd_soc_of_parse_audio_routing(&data->card, "audio-routing");
	if (ret)
		goto fail;
	data->card.num_links = 1;
	data->card.owner = THIS_MODULE;
	data->card.dai_link = &data->dai;
	data->card.dapm_widgets = imx_max98357a_dapm_widgets;
	data->card.num_dapm_widgets = ARRAY_SIZE(imx_max98357a_dapm_widgets);
	

	ret = snd_soc_register_card(&data->card);
	if (ret) {
		dev_err(&pdev->dev, "snd_soc_register_card failed (%d)\n", ret);
		goto fail;
	}

	platform_set_drvdata(pdev, data);
fail:
	if (cpu_np)
		of_node_put(cpu_np);
	if (codec_np)
		of_node_put(codec_np);

	return ret;

}

static int imx_max98357a_remove(struct platform_device *pdev){
	
	struct imx_max98357a_data *data = platform_get_drvdata(pdev);

	snd_soc_unregister_card(&data->card);

	return 0;
}

static const struct of_device_id imx_max98357a_dt_ids[] = {
	{ .compatible = "fsl,imx-audio-max98357a", },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, imx_max98357a_dt_ids);

static struct platform_driver imx_max98357a_driver = {
	.driver = {
		.name = "imx-amp-max98357a",
		.owner = THIS_MODULE,
		.pm = &snd_soc_pm_ops,
		.of_match_table = imx_max98357a_dt_ids,
	},
	.probe = imx_max98357a_probe,
	.remove = imx_max98357a_remove,
};

module_platform_driver(imx_max98357a_driver);

/* Module information */
MODULE_AUTHOR("Modelkin Igor");
MODULE_DESCRIPTION("ALSA SoC i.MX max98357a");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:imx-tuner-max98357a");
