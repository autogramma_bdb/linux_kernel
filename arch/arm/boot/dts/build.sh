#!/bin/bash

IDE=$1

if [ ! $IDE ]; then
	echo "usage: $0 <device-name>"
	exit 1
fi

SRC=$IDE.dts
TMP=$IDE.tmp.dts
DST=$IDE.dtb
DTC=../../../../scripts/dtc/dtc


echo "-- GCC link file $SRC to $TMP"
cpp -nostdinc -I../../../../include/ -undef -x assembler-with-cpp $SRC > $TMP
echo " "

echo "-- compiling $TMP"
$DTC -O dtb -b 0 -o $DST $TMP
echo " "

echo "-- decompiling $DST to $IDE.decompiled.dts with removed phandles"
$DTC -O dts -s -o $IDE.decompiled.dts.tmp $DST
echo " "

echo "-- del phandle in $IDE.decompiled.dts file"
cat $IDE.decompiled.dts.tmp | grep -vE "\sphandle|\slinux,phandle" > $IDE.decompiled.dts
rm $IDE.decompiled.dts.tmp
echo " "

echo "-- remove $TMP"
rm $TMP
echo " "

exit 0
